import React, { Component } from "react";
import "./style.css";

let arrProduct = [
  {
    id: 1,
    price: 30,
    name: "GUCCI G8850U",
    url: "./glassesImage/v1.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 2,
    price: 50,
    name: "GUCCI G8759H",
    url: "./glassesImage/v2.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 3,
    price: 30,
    name: "DIOR D6700HQ",
    url: "./glassesImage/v3.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 4,
    price: 30,
    name: "DIOR D6005U",
    url: "./glassesImage/v4.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 5,
    price: 30,
    name: "PRADA P8750",
    url: "./glassesImage/v5.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 6,
    price: 30,
    name: "PRADA P9700",
    url: "./glassesImage/v6.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 7,
    price: 30,
    name: "FENDI F8750",
    url: "./glassesImage/v7.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 8,
    price: 30,
    name: "FENDI F8500",
    url: "./glassesImage/v8.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 9,
    price: 30,
    name: "FENDI F4300",
    url: "./glassesImage/v9.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
];

class Glasses extends Component {
  constructor(props) {
    super(props);
    this.state = {
      glassesImg: "",
      glassesName: "",
      glassesDesc: "",
    };
  }

  changeGlasses = (img, name, desc) => () => {
    this.setState(
      {
        glassesImg: img,
        glassesName: name,
        glassesDesc: desc,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  render() {
    return (
      <div>
        <div className="bg-wrapper">
          <div className="bg-img"></div>
          <div className="overlay-dark"></div>
        </div>
        <div className="content-wrapper">
          <h4 className="text-white mt-5 text-center">
            TRY<span className="highlight">&ensp;GLASSES APP&ensp;</span>ONLINE
          </h4>

          {/* Model */}
          <div className="before-after">
            <div className="control-before">
              <label className="label-before px-2">Before</label>
              <img className="before" src={"/glassesImage/model.jpg"}></img>
            </div>

            <div className="control-after">
              <label className="label-after px-2">After</label>
              <img className="after" src={"/glassesImage/model.jpg"}></img>

              {/* Glasses Info */}
              <div className="glasses-info-text p-2">
                <div className='bgText'>
                  <h6 className="hName">{this.state.glassesName}</h6>
                  <p className="pDesc">{this.state.glassesDesc}</p>
                </div>
              </div>
              <img
                className="glasses-info-img"
                src={this.state.glassesImg}
              ></img>
            </div>
          </div>

          {/* Glasses Buttons Collection */}
          <div className="glasses-collection">
            <button
              onClick={this.changeGlasses(
                arrProduct[0].url,
                arrProduct[0].name,
                arrProduct[0].desc
              )}
              className="btn btn-outline-primary px-1"
            >
              <img className="gStyle v1" src={"/glassesImage/v1.png"}></img>
            </button>
            <button
              onClick={this.changeGlasses(
                arrProduct[1].url,
                arrProduct[1].name,
                arrProduct[1].desc
              )}
              className="btn btn-outline-primary px-1"
            >
              <img className="gStyle v1" src={"/glassesImage/v2.png"}></img>
            </button>
            <button
              onClick={this.changeGlasses(
                arrProduct[2].url,
                arrProduct[2].name,
                arrProduct[2].desc
              )}
              className="btn btn-outline-primary px-1"
            >
              <img className="gStyle v1" src={"/glassesImage/v3.png"}></img>
            </button>
            <button
              onClick={this.changeGlasses(
                arrProduct[3].url,
                arrProduct[3].name,
                arrProduct[3].desc
              )}
              className="btn btn-outline-primary px-1"
            >
              <img className="gStyle v1" src={"/glassesImage/v4.png"}></img>
            </button>
            <button
              onClick={this.changeGlasses(
                arrProduct[4].url,
                arrProduct[4].name,
                arrProduct[4].desc
              )}
              className="btn btn-outline-primary px-1"
            >
              <img className="gStyle v1" src={"/glassesImage/v5.png"}></img>
            </button>
            <button
              onClick={this.changeGlasses(
                arrProduct[5].url,
                arrProduct[5].name,
                arrProduct[5].desc
              )}
              className="btn btn-outline-primary px-1"
            >
              <img className="gStyle v1" src={"/glassesImage/v6.png"}></img>
            </button>
            <button
              onClick={this.changeGlasses(
                arrProduct[6].url,
                arrProduct[6].name,
                arrProduct[6].desc
              )}
              className="btn btn-outline-primary px-1"
            >
              <img className="gStyle v1" src={"/glassesImage/v7.png"}></img>
            </button>
            <button
              onClick={this.changeGlasses(
                arrProduct[7].url,
                arrProduct[7].name,
                arrProduct[7].desc
              )}
              className="btn btn-outline-primary px-1"
            >
              <img className="gStyle v1" src={"/glassesImage/v8.png"}></img>
            </button>
            <button
              onClick={this.changeGlasses(
                arrProduct[8].url,
                arrProduct[8].name,
                arrProduct[8].desc
              )}
              className="btn btn-outline-primary px-1"
            >
              <img className="gStyle v1" src={"/glassesImage/v9.png"}></img>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Glasses;
