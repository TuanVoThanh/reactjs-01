import React from "react";
import "./App.css";
import Home from "./cellPhones/Home";
import Glasses from "./glasses";

function App() {
  return (
    <div>
      {/* Project 1: Cell Phones */}
      {/* <Home /> */}

      {/* Project 2: Glasses App */}
      <Glasses />
    </div>
  );
}

export default App;

