import React, { Component } from "react";
import "./style.css";

class Footer extends Component {
  render() {
    return (
      <div>
        <h2>PROMOTION</h2>
        <div>
          <div className="bannerGroup">
            <div className="imgGroup row">
              <img className="col-sm-4" src={require("./img/promotion_1.png")}></img>
              <img className="col-sm-4" src={require("./img/promotion_2.png")}></img>
              <img className="col-sm-4" src={require("./img/promotion_3.jpg")}></img>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
