import React, { Component } from "react";
import "./style.css";
import Header from "../Header";
import Slider from "../Slider";
import ProductList from "../ProductList";
import Footer from "../Footer";

class Home extends Component {
  render() {
    return (
      <div className="home-wrapper">
        <Header />
        <Slider />
        <ProductList />
        <Footer />
      </div>
    );
  }
}

export default Home;
