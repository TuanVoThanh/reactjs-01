import React, { Component } from "react";
import "./style.css";
import Product from "../Product";

class ProductList extends Component {
  render() {
    return (
      <div>
        <h2>BEST SMART PHONE</h2>
        <div className="productList container-fluid">
          <Product />
        </div>
      </div>
    );
  }
}

export default ProductList;
